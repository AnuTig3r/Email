��          L      |       �   
   �      �      �      �   -   �     '     ?  ,   L  )   y     �  4   �                                         Send Email Send Email to Selected Students Send Email to Selected Users Subject You must configure the %s to use this script. Project-Id-Version: Email module
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-09 23:04+0100
PO-Revision-Date: 2018-11-09 23:04+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 1.8.11
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Enviar Email Enviar Email a los Estudiantes Seleccionados Enviar Email a los Usuarios Seleccionados Asunto Debe configurar el %s para poder usar este programa. 