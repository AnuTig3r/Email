��          L      |       �   
   �      �      �      �   -   �     '     >  +   L  /   x     �  ?   �                                         Send Email Send Email to Selected Students Send Email to Selected Users Subject You must configure the %s to use this script. Project-Id-Version: Email module
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-09 23:05+0100
PO-Revision-Date: 2018-11-09 23:05+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 1.8.11
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Envoyer Email Envoyer l'Email aux Élèves Sélectionnés Envoyer l'Email aux Utilisateurs Sélectionnés Sujet Vous devez configurer le %s pour pouvoir utiliser ce programme. 