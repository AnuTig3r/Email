Email module
============

![screenshot](https://gitlab.com/francoisjacquet/Email/raw/master/screenshot.png?inline=false)

https://gitlab.com/francoisjacquet/Email/

Version 3.0 - November, 2018

License GNU GPL v2

Author François Jacquet

Sponsored by Microtec Guatemala

DESCRIPTION
-----------
This additional module extends the following modules:

- Students: Send Email.
- Users: Send Email.

The email body is customizable.

A test email can be sent before sending to students or users.

CONTENT
-------
Students
- Send Emails

Users
- Send Emails

INSTALL
-------
Copy the `Email/` folder (if named `Email-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School Setup > School Configuration > Modules_ and click "Activate".

Requires RosarioSIS 3+
